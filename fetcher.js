
const globalOptions = {
    credentials: 'same-origin',
    redirect: 'error'
};

async function request(uri, method, headers, query, body, options) {
    let response, responseBody;

    const url = new URL(uri, typeof window !== 'undefined' ? window.location.origin : undefined);
    url.search = new URLSearchParams(query).toString();

    const fetchOptions = Object.assign({}, globalOptions, options, { method, headers });

    if (body instanceof FormData) {
        // let native fetch() determine content type
        delete headers['Content-Type'];
        fetchOptions.body = body;
    } else if (body) {
        headers['Content-Type'] = 'application/json';
        fetchOptions.body = JSON.stringify(body);
    }

    try {
        response = await fetch(url, fetchOptions);

        const headers = response.headers;
        const contentType = headers.get('Content-Type') || '';

        if (method === 'HEAD') {
            responseBody = '';
        } else {
            if (contentType.indexOf('application/json') !== -1) {
                try {
                    responseBody = await response.json();
                } catch (e) {
                    throw new Error(`Failed to parse response as json for content type ${contentType}.`, e);
                }
            } else {
                responseBody = await response.text();
            }
        }
    } catch (e) {
        throw e;
    }

    return { status: response.status, body: responseBody };
}

async function head(uri, query = {}, options = {}) {
    return await request(uri, 'HEAD', {}, query, null, options);
}

async function get(uri, query = {}, options = {}) {
    return await request(uri, 'GET', {}, query, null, options);
}

async function post(uri, body, query = {}, options = {}) {
    return await request(uri, 'POST', {}, query, body, options);
}

async function put(uri, body, query = {}, options = {}) {
    return await request(uri, 'PUT', {}, query, body, options);
}

async function del(uri, query = {}, options = {}) {
    return await request(uri, 'DELETE', {}, query, null, options);
}

export default {
    globalOptions,
    head,
    get,
    post,
    put,
    del,
    delete: del
};
