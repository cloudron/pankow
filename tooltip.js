/*

Tooltip Directive

```
import tooltip from 'pankow/tooltip';

app.directive('tooltip', tooltip);

...

<div v-tooltip.top="'hello'"></div>

```

*/

const TOP = 1;
const BOTTOM = 2;
const LEFT = 3;
const RIGHT = 4;

const padding = 10;

const tooltips = {};

function remove(key) {
    if (tooltips[key]) tooltips[key].remove();
    delete tooltips[key];
}

function update(target, value, modifiers, tooltip) {
    if (!tooltip) return;

    tooltip.innerText = value;

    // BOTTOM is default
    const pos = modifiers.top ? TOP : (modifiers.left ? LEFT : (modifiers.RIGHT ? RIGHT : BOTTOM));

    const targetRect = target.getBoundingClientRect();
    const tooltipRect = tooltip.getBoundingClientRect();

    if (pos === TOP || pos === BOTTOM) tooltip.style.left = (targetRect.left + targetRect.width/2) - (tooltipRect.width/2) + 'px';
    else if (pos === LEFT) tooltip.style.left = (targetRect.left - tooltipRect.width - padding) + 'px';
    else tooltip.style.left = (targetRect.left + targetRect.width + padding) + 'px';

    if (pos === LEFT || pos === RIGHT) tooltip.style.top = (targetRect.top + targetRect.height/2) - (tooltipRect.height/2) + 'px';
    else if (pos === TOP) tooltip.style.top = (targetRect.top - tooltipRect.height - padding) + 'px';
    else tooltip.style.top = (targetRect.bottom + padding) + 'px';
}

function mounted(el, binding, vnode) {
    el.addEventListener('mouseenter', () => {
        if (!binding.value) return remove(vnode.key);

        tooltips[vnode.key] = document.createElement('div');
        tooltips[vnode.key].classList.add('pankow-tooltip');
        window.document.body.appendChild(tooltips[vnode.key]);

        update(el, binding.value, binding.modifiers, tooltips[vnode.key]);
    });

    el.addEventListener('mouseleave', () => {
        remove(vnode.key);
    });
}

function updated(el, binding, vnode) {
    if (!binding.value) return remove(vnode.key);
    update(el, binding.value, binding.modifiers, tooltips[vnode.key]);
}

function beforeUnmount(el, binding, vnode) {
    remove(vnode.key);
}

const tooltip = {
    mounted,
    updated,
    beforeUnmount
};

export default tooltip;
