import GenericViewer from './viewers/GenericViewer.vue';
import ImageViewer from './viewers/ImageViewer.vue';
import PdfViewer from './viewers/PdfViewer.vue';
import TextViewer from './viewers/TextViewer.vue';

export {
    GenericViewer,
    ImageViewer,
    PdfViewer,
    TextViewer,
};
