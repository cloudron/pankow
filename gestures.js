/*

Swipe handler

```
import { onSwipe } from 'pankow/guesture';

onSwipe(element, (direction) => {});

```

*/
function onSwipe(elem, callback, threshold = 50) {
  callback = typeof callback === 'function' ? callback : () => {};

  let startX = 0;
  let startY = 0;
  let endX = 0;
  let endY = 0;

  function touchStart(event) {
    if (event.touches.length !== 1) return;
    startX = event.touches[0].clientX;
    startY = event.touches[0].clientY;
  };

  function touchMove(event) {
    if (event.touches.length !== 1) return;
    endX = event.touches[0].clientX;
    endY = event.touches[0].clientY;
  }

  function touchEnd(event) {
    const diffX = startX - endX;
    const diffY = startY - endY;
    const absX = Math.abs(diffX);
    const absY = Math.abs(diffY);

    if (absX < threshold && absY < threshold) return;

    // horizontal
    if (absX > absY) return callback(diffX > 0 ? 'left' : 'right');

    // vertical
    callback(diffY > 0 ? 'up' : 'down');
  };

  elem.addEventListener('touchstart', touchStart);
  elem.addEventListener('touchmove', touchMove);
  elem.addEventListener('touchend', touchEnd);
  elem.addEventListener('touchcancel', touchEnd);
}

export default {
  onSwipe
};

export {
  onSwipe
};
