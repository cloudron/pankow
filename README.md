## Pankow

Pankow is the UI component library for scaffolding Cloudron apps. It is based on [Vue.js](https://vuejs.org/) using [Vite](https://vitejs.dev/) as the bundler.
The main use-case is to provide common Cloudron related UI compents like a login screen and common layout with header, body and footers.

### Getting started

Open a terminal in the Cloudron app main folder. Run the following command to init a subfolder where the UI project will be contained.
For consistency the project name here is `frontend`, so make sure to specify that as the first question asked in the init wizard.
All other options can be accpeted as defaults.

```sh
npm init vue@latest
cd frontend
npm install
npm run dev
```

This will install and build the initial project with the default Vue.js Hello World page.

