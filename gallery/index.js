import { createApp } from 'vue';

import Index from './Index.vue';
import tooltip from '../tooltip.js';

const app = createApp(Index);

app.directive('tooltip', tooltip);

app.mount('#app');
