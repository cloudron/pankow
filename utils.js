
import { filesize } from 'filesize';
import { customRef } from 'vue';

// https://vuejs.org/api/reactivity-advanced.html#customref
function useDebouncedRef(value, delay = 300) {
  let timeout
  return customRef((track, trigger) => {
    return {
      get() {
        track()
        return value
      },
      set(newValue) {
        clearTimeout(timeout)
        timeout = setTimeout(() => {
          value = newValue
          trigger()
        }, delay)
      }
    }
  })
}

function getFileTypeGroup(item) {
    if (typeof item.mimeType !== 'string') throw 'item must have mimeType string property';
    return item.mimeType.split('/')[0];
}

function isValidDomainOrURL(input) {
    const domainRegex = /^([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}$/;

    let domain;
    try {
        domain = new URL(input).hostname;
    } catch (e) {
        domain = input
    }

    return domainRegex.test(domain);
}

// https://en.wikipedia.org/wiki/Binary_prefix
// binary units (IEC) 1024 based
function prettyBinarySize(size, fallback) {
    if (!size) return fallback || 0;
    if (size === -1) return 'Unlimited';

    // we can also use KB here (JEDEC)
    var i = Math.floor(Math.log(size) / Math.log(1024));
    return (size / Math.pow(1024, i)).toFixed(3) * 1 + ' ' + ['B', 'KiB', 'MiB', 'GiB', 'TiB'][i];
}

// decimal units (SI) 1000 based
function prettyDecimalSize(size, fallback) {
    if (!size) return fallback || 0;

    var i = Math.floor(Math.log(size) / Math.log(1000));
    return (size / Math.pow(1000, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

function prettyDate(value) {
    if (!value) return 'never';

    const date = new Date(value),
    diff = (((new Date()).getTime() - date.getTime()) / 1000),
    day_diff = Math.floor(diff / 86400);

    if (isNaN(day_diff) || day_diff < 0)
        return;

    return day_diff === 0 && (
        diff < 60 && 'just now' ||
        diff < 120 && '1 min ago' ||
        diff < 3600 && Math.floor( diff / 60 ) + ' min ago' ||
        diff < 7200 && '1 hour ago' ||
        diff < 86400 && Math.floor( diff / 3600 ) + ' hours ago') ||
        day_diff === 1 && 'Yesterday' ||
        day_diff < 7 && day_diff + ' days ago' ||
        day_diff < 31 && Math.ceil( day_diff / 7 ) + ' weeks ago' ||
        day_diff < 365 && Math.round( day_diff / 30 ) +  ' months ago' ||
        Math.round( day_diff / 365 ) + ' years ago';
}

function prettyLongDate(value) {
    if (!value) return 'unknown';

    const date = new Date(value);
    return date.toLocaleString();
}

function prettyFileSize(value) {
    if (typeof value !== 'number') return 'unknown';

    return filesize(value);
}

function prettyEmailAddresses(addresses) {
    if (!addresses) return '';
    if (addresses === '<>') return '<>';
    if (Array.isArray(addresses)) return addresses.map(function (a) { return a.slice(1, -1); }).join(', ');
    return addresses.slice(1, -1);
}

function sanitize(path) {
    path = '/' + path;
    return path.replace(/\/+/g, '/');
}

function pathJoin() {
    return sanitize(Array.from(arguments).join('/'));
}

function download(entries, name) {
    if (!entries.length) return;

    if (entries.length === 1) {
        if (entries[0].share) window.location.href = '/api/v1/shares/' + entries[0].share.id + '?type=download&path=' + encodeURIComponent(entries[0].filePath);
        else window.location.href = '/api/v1/files?type=download&path=' + encodeURIComponent(entries[0].filePath);

        return;
    }

    const params = new URLSearchParams();

    // be a bit smart about the archive name and folder tree
    const folderPath = entries[0].filePath.slice(0, -entries[0].fileName.length);
    const archiveName = name || folderPath.slice(folderPath.slice(0, -1).lastIndexOf('/')+1).slice(0, -1);
    params.append('name', archiveName);
    params.append('skipPath', folderPath);

    params.append('entries', JSON.stringify(entries.map(function (entry) {
        return {
            filePath: entry.filePath,
            shareId: entry.share ? entry.share.id : undefined
        };
    })));

    window.location.href = '/api/v1/download?' + params.toString();
}

// simple extension detection, does not work with double extension like .tar.gz
function getExtension(entry) {
    if (entry.isFile) return entry.fileName.slice(entry.fileName.lastIndexOf('.') + 1);
    return '';
}

function copyToClipboard(value) {
    var elem = document.createElement('input');
    elem.value = value;
    document.body.append(elem);
    elem.select();
    document.execCommand('copy');
    elem.remove();
}

function urlSearchQuery() {
    return decodeURIComponent(window.location.search).slice(1).split('&').map(function (item) { return item.split('='); }).reduce(function (o, k) { o[k[0]] = k[1]; return o; }, {});
}

// those paths contain the internal type and path reference eg. shares/:shareId/folder/filename or files/folder/filename
function parseResourcePath(resourcePath) {
    var result = {
        type: '',
        path: '',
        shareId: '',
        apiPath: '',
        resourcePath: ''
    };

    if (resourcePath.indexOf('files/') === 0) {
        result.type = 'files';
        result.path = resourcePath.slice('files'.length) || '/';
        result.apiPath = '/api/v1/files';
        result.resourcePath = result.type + result.path;
    } else if (resourcePath.indexOf('shares/') === 0) {
        result.type = 'shares';
        result.shareId = resourcePath.split('/')[1];
        result.path = resourcePath.slice((result.type + '/' + result.shareId).length) || '/';
        result.apiPath = '/api/v1/shares/' + result.shareId;
        // without shareId we show the root (share listing)
        result.resourcePath = result.type + '/' + (result.shareId ? (result.shareId + result.path) : '');
    } else {
        console.error('Unknown resource path', resourcePath);
    }

    return result;
}

function getEntryIdentifier(entry) {
    return (entry.share ? (entry.share.id + '/') : '') + entry.filePath;
}

function entryListSort(list, prop, desc) {
    var tmp = list.sort(function (a, b) {
        var av = a[prop];
        var bv = b[prop];

        if (typeof av === 'string') return (av.toUpperCase() < bv.toUpperCase()) ? -1 : 1;
        else return (av < bv) ? -1 : 1;
    });

    if (desc) return tmp;
    return tmp.reverse();
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// this is from the Cloudron dashboard translation project en.json
const fallbackTranslations = {
    "main": {
        "dialog": {
            "close": "Close"
        }
    },
    "filemanager": {
        "title": "File Manager",
        "removeDialog": {
            "reallyDelete": "Really delete the following?"
        },
        "newDirectoryDialog": {
            "title": "New Folder",
            "create": "Create"
        },
        "newFileDialog": {
            "title": "New File",
            "create": "Create"
        },
        "renameDialog": {
            "title": "Rename {{ fileName }}",
            "newName": "New Name",
            "rename": "Rename"
        },
        "chownDialog": {
            "title": "Change ownership",
            "newOwner": "New Owner",
            "change": "Change Owner",
            "recursiveCheckbox": "Change ownership recursively"
        },
        "uploadingDialog": {
            "title": "Uploading files ({{ countDone }}/{{ count }})",
            "errorAlreadyExists": "One or more files already exist.",
            "errorFailed": "Failed to upload one or more files. Please try again.",
            "closeWarning": "Do not refresh the page until upload has finished.",
            "retry": "Retry",
            "overwrite": "Overwrite"
        },
        "extractDialog": {
            "title": "Extracting {{ fileName }}",
            "closeWarning": "Do not refresh the page until extract has finished."
        },
        "textEditorCloseDialog": {
            "title": "File has unsaved changes",
            "details": "Your changes will be lost if you don't save them",
            "dontSave": "Don't Save"
        },
        "notFound": "Not found",
        "toolbar": {
            "new": "New",
            "upload": "Upload",
            "newFile": "New File",
            "newFolder": "New Folder",
            "uploadFolder": "Upload Folder",
            "uploadFile": "Upload File",
            "restartApp": "Restart App",
            "openTerminal": "Open Terminal",
            "openLogs": "Open Logs"
        },
        "list": {
            "name": "Name",
            "size": "Size",
            "owner": "Owner",
            "empty": "No files",
            "symlink": "symlink to {{ target }}",
            "menu": {
                "rename": "Rename",
                "chown": "Change Ownership",
                "extract": "Extract Here",
                "download": "Download",
                "delete": "Delete",
                "edit": "Edit",
                "cut": "Cut",
                "copy": "Copy",
                "paste": "Paste",
                "selectAll": "Select All",
                "share": "Share",
                "open": "Open"
            },
            "mtime": "Modified"
        },
        "extract": {
            "error": "Failed to extract: {{ message }}"
        },
        "newDirectory": {
            "errorAlreadyExists": "Already exists"
        },
        "newFile": {
            "errorAlreadyExists": "Already exists"
        },
        "status": {
            "restartingApp": "restarting app"
        },
        "uploader": {
            "uploading": "Uploading",
            "exitWarning": "Upload still in progress. Really close this page?"
        },
        "textEditor": {
            "undo": "Undo",
            "redo": "Redo",
            "save": "Save"
        },
        "extractionInProgress": "Extraction in progress",
        "pasteInProgress": "Pasting in progress",
        "deleteInProgress": "Deletion in progress"
    }
};

function translation(id) {
    let value;
    try {
        value = id.split('.').reduce((a, b) => a[b], fallbackTranslations);
    } catch (e) {
        console.warn(`Translation ${id} does not exist`);
        value = id;
    }

    return value;
}

function uuidv4() {
  return '10000000-1000-4000-8000-100000000000'.replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}

// named exports
export {
    getFileTypeGroup,
    translation,
    useDebouncedRef,
    isValidDomainOrURL,
    prettyBinarySize,
    prettyDecimalSize,
    prettyDate,
    prettyLongDate,
    prettyFileSize,
    prettyEmailAddresses,
    sanitize,
    pathJoin,
    download,
    getExtension,
    copyToClipboard,
    urlSearchQuery,
    parseResourcePath,
    getEntryIdentifier,
    entryListSort,
    sleep,
    uuidv4
};

// default export
export default {
    getFileTypeGroup,
    translation,
    useDebouncedRef,
    isValidDomainOrURL,
    prettyBinarySize,
    prettyDecimalSize,
    prettyDate,
    prettyLongDate,
    prettyFileSize,
    prettyEmailAddresses,
    sanitize,
    pathJoin,
    download,
    getExtension,
    copyToClipboard,
    urlSearchQuery,
    parseResourcePath,
    getEntryIdentifier,
    entryListSort,
    sleep,
    uuidv4
};
