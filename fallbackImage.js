/*

FallbackImage Directive

```
import fallbackImage from 'pankow/fallbackImage';

app.directive('fallback-image', fallbackImage);

...

<im v-fallback-image="'/fallback.png'"/>

```

*/

function mounted(el, binding, vnode) {
  el.addEventListener('error', () => {
    console.log('image loading failed, try to load', binding.value);
    el.src = binding.value;
  });
}

const fallbackImage = {
  mounted
};

export default fallbackImage;
